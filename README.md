# Soal-Shift-Sisop-Modul-1-C11-2022

## Daftar Isi ##
- [Daftar Anggota Kelompok](#daftar-anggota-kelompok)
- [Nomer 1](#nomer-1)
    - [Soal  1.A](#soal-1a)
    - [Soal  1.B](#soal-1b)
    - [Soal  1.C](#soal-1c)
    - [Soal  1.D](#soal-1d)
- [Nomer 2](#nomer-2)
    - [Soal  2.B](#soal-2b)
    - [Soal  2.C](#soal-2b)
    - [Soal  2.D](#soal-2c)
    - [Soal  2.E](#soal-2d)
- [Kesulitan](#kesulitan)


## Daftar Anggota Kelompok ##

NRP | NAMA | KELAS
--- | --- | ---
5025201094  | Reyner Fernaldi | SISOP C
505201030  | Mohammad Nouval Bachrezi | SISOP C

## Nomer 1 ##
Selama user gagal melakukan registrasi, maka sistem akan terus meminta username dan pasword
```bash
while [ $loginStatus == false ]
do
	echo "Masukkan Username:"
	read username
	echo "Masukkan Password:"
	read -s password

```
Kemudian dilakukan validasi username untuk mengecek apakah username yang didaftarkan belum terdaftar pada users/user.txt. Konsepnya adalah dengan melooping semua string yang diawali dengan "Username:" pada user.txt. Jika didapati ada yang sama, maka validasi username akan gagal.
```bash
	#Validasi Username
	for user in ${path[@]}
	do
		if [[ $user == "Username:"* ]]
		then
		temp="Username:${username}"
			if [[ "$user" == $temp ]]
			then
				echo "user sudah ada"
				echo "$(date +'%m/%d/%y') $(date +'%r') REGISTER: ERROR User already exists" >> ./log.txt
				usernamecheck=false
				break
			fi
		fi
	done

```
Untuk login, kita harus melakukan validasi username terlebih dahulu, apakah username yang dimasukkan terdapat pada folder user.txt. Jika ada, maka dilakukan validasi password. Konsep dari validasi password adalah mengambil next line dari username. Maksudnya adalah, password untuk suatu username berada dibawah username tersebut pada user.txt. Oleh karena itu, ketika looping validasi username, kita dapat menggunakan index. Sehingga ketika didapati username yang sesuai, kita tinggal mengambil string yang dibawahnya untuk dijadikan validasi password.

```bash
	if [[ $usernamecheck == false ]]
	then
		for user in ${path[@]}
		do
		let n=n+1
			if [[ $user == "Username:"* ]]
			then
				#Jika username valid
				if [[ "$user" == $tempUsername ]]
				then
					usernamecheck=true
					break
				fi
			fi
		done
	fi
	#jika uername tidak ada di daftar
	if [[ $usernamecheck == false ]]
	then
		echo "Username tidak ditemukan"
	fi
	#validasi password
	if [[ $usernamecheck == true ]]
	then

		for user in ${path[@]} 
		do
			let place=$n+1
			let index=index+1
			if [[ $index == $place ]]
			then
				#Username dan password valid
				if [[ "$user" == $tempPassword ]]
				then
					echo "ok login sukses"
					echo "$(date +'%m/%d/%y') $(date +'%r') LOGIN: INFO User ${username} logged in" >> ./log.txt
					loginStatus=true
					break
				else
					echo "password salah!"
					echo "$(date +'%m/%d/%y') $(date +'%r') LOGIN: ERROR Failed login attempt on user ${username}" >> ./log.txt
					break
				fi
			fi
		done
	fi
done

```

### Soal 1.a ###
Membuat 2 buah file berekstensi .sh, yaitu register.sh untuk registrasi dan main.sh untuk login

### Soal 1.b ###
Jika sudah melakukan validasi username, maka dilakukan validasi password dengan menggunakan percabangan if.
```bash
	#Validasi Password
	if [ $usernamecheck == true ]
	then
		if [ $username == $password ]
		then
			echo "password sama dengan username!"
		elif [ $passwordLength -lt $minimalCharacter ]
		then
			echo "Password minimal 8 karakter"
		elif [[ $password =~ ['!@#$%^&*()_+'] ]]
		then
			echo "Password harus alphanumerik!"
		elif [[ "$password" =~ [[:upper:]] ]]
		then
			if [[ "$password" =~ [[:lower:]] ]]
			then
				echo "Login Success!"
				echo "Username:${username}" >> ./users/user.txt
				echo "Password:${password}" >> ./users/user.txt
				
				echo "$(date +'%m/%d/%y') $(date +'%r') REGISTER:INFO User ${username} registered successfully" >> ./log.txt
				loginStatus=true
			else
				echo "Password harus mengandung huruf kapital dan huruf kecil!"
			fi
		else
		echo "Password harus mengandung huruf kapital dan huruf kecil!"
		fi
	fi
done

```

### Soal 1.c ###
Memasukkan history register dan login dengan
```bash
echo "message" >> file_yang_dituju
```
seperti
```bash
echo "$(date +'%m/%d/%y') $(date +'%r') REGISTER: ERROR User already exists" >> ./log.txt
echo "$(date +'%m/%d/%y') $(date +'%r') REGISTER:INFO User ${username} registered successfully" >> ./log.txt
echo "$(date +'%m/%d/%y') $(date +'%r') LOGIN: INFO User ${username} logged in" >> ./log.txt
echo "$(date +'%m/%d/%y') $(date +'%r') LOGIN: ERROR Failed login attempt on user ${username}" >> ./log.txt
```

### Soal 1.d ###
Menggunakan percabangan if untuk menentukan aksi. Jika diawali dengan "dl", maka akan mengecek apakah ada file yang sama atau tidak. Jika ada maka akan di remove. Setelah itu membuat folder yang berisi foto dengan format nama
```
$(date +'%F')_${username}
```

```bash
	if [[ $input == "dl"* ]]
	then
		#Kalau sudah ada filenya
		if [[ -e "$(date +'%F')_${username}.zip" ]]
		then
			rm -r $(date +'%F')_${username}.zip
		fi
		#DOwnload file
		input=${input#*"dl "}
		mkdir "$(date +'%F')_${username}"
		for ((i=1; i<=$input; i=i+1))
		do
			wget -O $(date +'%F')_${username}/PIC_${i} https://loremflickr.com/320/240;
		done 
		zip -P ${password} -r $(date +'%F')_${username} $(date +'%F')_${username}
		rm -r $(date +'%F')_${username}

```
Jika aksi yang diinginkan adalah "att", maka dilakukan grep pada log.txt untuk mencari kata kunci "LOGIN" dan username yang digunakan

```bash
	elif [[ $input == "att" ]]
	then
		totalLogin=$(grep "LOGIN" log.txt | grep -c "$username")
		echo "$username telah melakukan percobaan login sebanyak $totalLogin kali"

```

## Nomer 2 ##
### Soal 2.b ###
Karena data log yang diberikan hanya dalam rentang 12 jam, maka kita hanya perlu menghitung total request dibagi dengan 12, sehingga mendapatkan rata-rata per jam
```bash
#Mancari rata-rata
cat ./log_website_daffainfo.log | awk '
	BEGIN{ 
		FS=":" 
		total=0}
	{total++}
	END{
		total=total/12
		print "Rata-rata serangan adalah sebanyak", total, "requests per jam"
	}
	
'>> ./ratarata.txt
```

### Soal 2.c ###
Untuk menampilkan IP yang paling banyak, maka kita dapat memanfaatkan array. Array pada awk menggunakan sistem asosiatif, yaitu sistem key and value. Sehingga kita jadikan IP sebagai key dan value sebagai jumlah request dari IP tersebut. Selanjutnya hanya tinggal mencari IP yang paling banyak berdasarkan value pada array tersebut
```bash
#Mencari ip paling banyak
cat ./log_website_daffainfo.log | awk '
	BEGIN{
		FS=":"
		reqip=""
		reqamount=0}
	{arr[$1]+=1}
	END{

		for (i in arr){
			if (reqamount < arr[i]){
				reqip = i
				reqamount = arr[reqip]
			}
		}
		print "yang paling banyak mengakses server adalah: " reqip " sebanyak " reqamount " request\n"
	}
'>> ./result.txt

```

### Soal 2.d ###
Untuk mencari user-agent curl, kita dapat langsung menggunakan awk dengan kata kunci
```bash
/curl/
```
kemudian mengiterasi jumlah kemunculannya
```bash
#Mencari banyak request curl
cat ./log_website_daffainfo.log | awk '
BEGIN{}
	/curl/ {count++} 
	END{
		print "ada " count " request yang menggunakan curl sebagai user-agent\n"}

'>> ./result.txt

```

### Soal 2.e ###
Untuk mencari request pada jam 2 pagi, kita dapat mencari pada field jam, dimana jam yang kita cari adalah jam "02". Tentunya pada satu baris string harus kita bagi-bagi kedalam beberapa field dengan separator ":"
```bash
#Mencari ip yang login di jam 2
cat ./log_website_daffainfo.log | awk '
	BEGIN{
		FS=":"}
	{
		if ($3=="02"){
			if(arr[$1]==0){
				arr[$1]=1
			}
			if(arr[$1]==1){
				print $1" Jam 2 Pagi"
				arr[$1]=2
			}
		}
	}
	END{}
'>> ./result.txt
```
### Kesulitan ###
Proses ngoding menjadi lama karena belum terbiasa dengan sintaks bash shell scripting. Masih sering error sintaks hanya karena spasi ataupun karena newline (terutama pada awk)


Terima kasih
