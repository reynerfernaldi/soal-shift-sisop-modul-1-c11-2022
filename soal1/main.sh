#!/bin/bash
inputValidate=false
loginStatus=false
minimalCharacter=8
path=(`cat ./users/user.txt`)

#looping sampai username dan passwor benar
while [ $loginStatus == false ]
do
	usernamecheck=false
	n=-1
	index=-1
	
	echo "Masukkan Username:"
	read username
	echo "Masukkan Password:"
	read -s password
	
	tempUsername="Username:${username}"
	tempPassword="Password:${password}"
	#validasi username
	if [[ $usernamecheck == false ]]
	then
		for user in ${path[@]}
		do
		let n=n+1
			if [[ $user == "Username:"* ]]
			then
				#Jika username valid
				if [[ "$user" == $tempUsername ]]
				then
					usernamecheck=true
					break
				fi
			fi
		done
	fi
	#jika uername tidak ada di daftar
	if [[ $usernamecheck == false ]]
	then
		echo "Username tidak ditemukan"
	fi
	#validasi password
	if [[ $usernamecheck == true ]]
	then

		for user in ${path[@]} 
		do
			let place=$n+1
			let index=index+1
			if [[ $index == $place ]]
			then
				#Username dan password valid
				if [[ "$user" == $tempPassword ]]
				then
					echo "ok login sukses"
					echo "$(date +'%m/%d/%y') $(date +'%r') LOGIN: INFO User ${username} logged in" >> ./log.txt
					loginStatus=true
					break
				else
					echo "password salah!"
					echo "$(date +'%m/%d/%y') $(date +'%r') LOGIN: ERROR Failed login attempt on user ${username}" >> ./log.txt
					break
				fi
			fi
		done
	fi
done

while [[ $inputValidate == false ]]
do
	read input

	if [[ $input == "dl"* ]]
	then
		#Kalau sudah ada filenya
		if [[ -e "$(date +'%F')_${username}.zip" ]]
		then
			rm -r $(date +'%F')_${username}.zip
		fi
		#DOwnload file
		input=${input#*"dl "}
		mkdir "$(date +'%F')_${username}"
		for ((i=1; i<=$input; i=i+1))
		do
			wget -O $(date +'%F')_${username}/PIC_${i} https://loremflickr.com/320/240;
		done 
		zip -P ${password} -r $(date +'%F')_${username} $(date +'%F')_${username}
		rm -r $(date +'%F')_${username}


	elif [[ $input == "att" ]]
	then
		totalLogin=$(grep "LOGIN" log.txt | grep -c "$username")
		echo "$username telah melakukan percobaan login sebanyak $totalLogin kali"
		
	elif [[ $input == "exit" ]]
	then
		echo "Terima kasih!"
		break
	fi
	echo "Ketik exit untuk keluar!"
done









