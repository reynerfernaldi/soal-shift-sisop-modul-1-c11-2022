#!/bin/bash

usernamecheck=true
loginStatus=false
minimalCharacter=8
path=(`cat ./users/user.txt`)

while [ $loginStatus == false ]
do
	echo "Masukkan Username:"
	read username
	echo "Masukkan Password:"
	read -s password
	
	passwordLength=${#password}
	#Validasi Username
	for user in ${path[@]}
	do
		if [[ $user == "Username:"* ]]
		then
		temp="Username:${username}"
			if [[ "$user" == $temp ]]
			then
				echo "user sudah ada"
				echo "$(date +'%m/%d/%y') $(date +'%r') REGISTER: ERROR User already exists" >> ./log.txt
				usernamecheck=false
				break
			fi
		fi
	done
	#Validasi Password
	if [ $usernamecheck == true ]
	then
		if [ $username == $password ]
		then
			echo "password sama dengan username!"
		elif [ $passwordLength -lt $minimalCharacter ]
		then
			echo "Password minimal 8 karakter"
		elif [[ $password =~ ['!@#$%^&*()_+'] ]]
		then
			echo "Password harus alphanumerik!"
		elif [[ "$password" =~ [[:upper:]] ]]
		then
			if [[ "$password" =~ [[:lower:]] ]]
			then
				echo "Login Success!"
				echo "Username:${username}" >> ./users/user.txt
				echo "Password:${password}" >> ./users/user.txt
				
				echo "$(date +'%m/%d/%y') $(date +'%r') REGISTER:INFO User ${username} registered successfully" >> ./log.txt
				loginStatus=true
			else
				echo "Password harus mengandung huruf kapital dan huruf kecil!"
			fi
		else
		echo "Password harus mengandung huruf kapital dan huruf kecil!"
		fi
	fi
done

	
