#!/bin/bash

#Mancari rata-rata
cat ./log_website_daffainfo.log | awk '
	BEGIN{ 
		FS=":" 
		total=0}
	{total++}
	END{
		total=total/12
		print "Rata-rata serangan adalah sebanyak", total, "requests per jam"
	}
	
'>> ./ratarata.txt

#Mencari ip paling banyak
cat ./log_website_daffainfo.log | awk '
	BEGIN{
		FS=":"
		reqip=""
		reqamount=0}
	{arr[$1]+=1}
	END{

		for (i in arr){
			if (reqamount < arr[i]){
				reqip = i
				reqamount = arr[reqip]
			}
		}
		print "yang paling banyak mengakses server adalah: " reqip " sebanyak " reqamount " request\n"
	}
'>> ./result.txt

#Mencari banyak request curl
cat ./log_website_daffainfo.log | awk '
BEGIN{}
	/curl/ {count++} 
	END{
		print "ada " count " request yang menggunakan curl sebagai user-agent\n"}

'>> ./result.txt


#Mencari ip yang login di jam 2
cat ./log_website_daffainfo.log | awk '
	BEGIN{
		FS=":"}
	{
		if ($3=="02"){
			if(arr[$1]==0){
				arr[$1]=1
			}
			if(arr[$1]==1){
				print $1" Jam 2 Pagi"
				arr[$1]=2
			}
		}
	}
	END{}
'>> ./result.txt



